#ifndef _DArray_h
#define _DArray_h

#include <stdlib.h>
#include <assert.h>
#include <lcthw/dbg.h>


typedef struct DArray {
    int end;
    int max;
    size_t element_size;
    size_t expand_rate;
    void **contents;
} DArray;


/**
 *  Create the array
 *
 *  @param element_size - the size of each element in bytes
 *  @param initial_max - the maximum amount of elements
 *
 *  @return A pointer to the new DArray
 */
DArray *DArray_create(size_t element_size, size_t initial_max);


/**
 * Destroy an array
 *
 * @param array - The DArray to be destroyed
 *
 * This function will nullify all contents of a DArray and then erase
 * the array.
 */
void DArray_destroy(DArray *array);


void DArray_clear(DArray *array);


int DArray_expand(DArray *array);


int DArray_contract(DArray *array);


/**
 * Push an element onto the array
 *
 * @param array - the DArray to operate on
 * @param el - the element to append
 *
 * @return success or failure
 *
 * This function will push element el onto the end of DArray array.
 */
int DArray_push(DArray *array, void *el);


/**
 * Pop an element off of the array
 *
 * @param array - the DArray to operate on
 *
 * @return the last element
 *
 * This function will remove and return the last element from DArray
 * array.
 */
void *DArray_pop(DArray *array);


void DArray_clear_destroy(DArray *array);


#define DArray_last(A) ((A)->contents[(A)->end - 1])
#define DArray_first(A) ((A)->contents[0])
#define DArray_end(A) ((A)->end)
#define DArray_count(A) DArray_end(A)
#define DArray_max(A) ((A)->max)

#define DEFAULT_EXPAND_RATE 300


static inline void DArray_set(DArray *array, int i, void *el)
{
    check(i < array->max, "darry attempt to set past max");
    if (i > array->end) { array->end = i; }
    array->contents[i] = el;
error:
    return;
}


/**
 * Get an item from an array
 *
 * @param array - the DArray to operate on
 * @param i - the index of the item to retreive
 *
 * @return the item at position i
 *
 * This function takes a DArray and an index and will return the array
 * item at the given index. It will not modify the array itself.
 */
static inline void *DArray_get(DArray *array, int i)
{
    check(i < array->max, "darray attempt to get past max");
    return array->contents[i];
error:
    return NULL;
}


/**
 * Remove an item form an array
 *
 * @param array - the DArray to operate on
 * @param i - the index of the item to remove
 *
 * @return the item at position i
 *
 * This function works the same as DArray_get except the item will be
 * removed from the array and returned.
 */
static inline void *DArray_remove(DArray *array, int i)
{
    void *el = array->contents[i];

    array->contents[i] = NULL;

    return el;
}


static inline void *DArray_new(DArray *array)
{
    check(array->element_size > 0, "Can't use DArray_new on 0 size darrys");

    return calloc(1, array->element_size);

error:
    return NULL;
}


/**
 * Concatenate two DArrays together
 * 
 * @param first - the first DArray to operate on
 * @param second - the second DArray to operate on
 *
 * This function will concatenate two DArrays into one DArray.
 */
void DArray_concat(DArray *first, DArray *second);


static inline void DArray_toast()
{
    printf("toast?????\n");
}


static inline void DArray_print(DArray *array)
{
    int i;

    if (array->element_size > 0) {
        for (i = 0; i < DArray_count(array); i++) {
            printf("[%d]", i);
            if (array->contents[i] != NULL) {
                printf("(%s)->", (char) array->contents[i]);
            }
        }
    }
    else {
        printf("trying to print array but its size is <= 0");
    }
}

#define DArray_free(E) free((E))

#endif
