#ifndef lcthw_List_h
#define lcthw_List_h

#include <stdlib.h>

struct ListNode;

typedef struct ListNode {
    struct ListNode *prev;
    struct ListNode *next;
    void *value;
} ListNode;

typedef struct List {
    int count;
    ListNode *first;
    ListNode *last;
} List;


/**
 * Create a new list.
 *
 * @return a pointer to the list
 *
 * Allocate the memory for a new list.
 */
List *List_create();


/**
 * Destroy a list.
 *
 * @param list - the List to operate on
 */
void List_destroy(List *list);


/**
 * Clear a list
 *
 * @param list - the List to operate on
 *
 * Will set all values to null.
 */
void List_clear(List *list);


/**
 * Clear and destroy a list
 *
 * @param list - the List to operate on
 *
 * Will clear all list nodes and then destroy the list.
 */
void List_clear_destroy(List *list);


#define List_count(A) ((A)->count)
#define List_first(A) ((A)->first != NULL ? (A)->first->value : NULL)
#define List_last(A) ((A)->last != NULL ? (A)->last->value : NULL)


void List_push(List *list, void *value);


void *List_pop(List *list);


void List_unshift(List *list, void *value);


void *List_shift(List *list);


void *List_remove(List *list, ListNode *node);


/**
 * Copy a list
 *
 * @param list - the list to copy
 *
 * @return a pointer to the new copy of the list
 */
void *List_copy(List *list);


/**
 *  Join two lists together
 *
 *  @param first - the first list
 *  @param second - the second list
 *
 *  `first` will be joined to `second`
 */
void List_join(List *first, List *second);


/**
 * Split a list at a node
 *
 *  @param list - the list
 *  @param value - the value to split on
 *
 *  @return a pointer to the split list
 *
 *  If there are duplicate values in a list, this will split on the first
 *  instance of the given value.
 */
void *List_split(List *list, void *value);


/**
 *  Print a list
 *
 *  @param list - the list to print
 */
void List_print(List *list);


/**
 *  Swaps two list members
 *
 *  @param list - the list containing the two members
 *  @param first - the first ListNode to swap
 *  @param second - the second ListNode to swap
 */
void List_swap(List *list, ListNode *first, ListNode *second);


/**
 *  Return the value at a given list index
 *
 *  @param list - the list itself
 *  @param index - the index of the value to return
 */
ListNode *List_value(List *list, int index);


#define LIST_FOREACH(L, S, M, V) ListNode *_node = NULL;\
    ListNode *V = NULL;\
    for(V = _node = L->S; _node != NULL; V = _node = _node->M)

#endif
