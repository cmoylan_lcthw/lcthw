#include <lcthw/heap.h>
#include <lcthw/dbg.h>
#include <lcthw/darray.h>


// TODO: take in the size of the heap in params
DArray *Heap_create()
{
    DArray *heap = DArray_create(sizeof(int), 5);

    return heap;
}


// TODO: this will return a void pointer eventually
int Heap_parent(int heap[], int index)
{
    // TODO: seems like a bad hack...
    index += 1;
    return heap[Heap_parent_index(index)];
}


int Heap_left(int heap[], int index)
{
    // TODO: bad hack
    index += 1;
    return heap[Heap_left_index(index)];
}


int Heap_right(int heap[], int index)
{
    // TODO: bad hack
    index += 1;
    return heap[Heap_right_index(index)];
}


void Heap_heapify(int heap[], int index)
{
    //int left = Heap_left(heap, index);
    //int right = Heap_right(heap, index);
    //int node = heap[index];
    //int heap_size = sizeof(heap);

    //if (node
}


void Heap_add(int heap[], int element)
{
    // TODO: need to use a dynamic structure so heap can grow and shrink
    int index = (sizeof(heap) + 1);

    // insert element to end of heap array
    heap[sizeof(heap) + 1] = element;

    // bubble up if parent < element
    int parent_index = Heap_parent_index(index);
    Heap_bubble(heap, index);
    // probably just call heapify here
    //if (parent < element) {
    //    int parent_index = Heap_parent_index(index);
    //    // swap
    //    heap[parent_index] = element;
    //    heap[index] = parent;
    //}

    debug("last element of heap is %i", heap[sizeof(heap) + 1]);
}


void Heap_bubble(int heap[], int index)
{
    int element;
    int parent_index;
    int parent;

    // TODO: fix the heap index thing
    // If index is < 1, we are at the top of the heap
    if (index > 1) {
        element = heap[index];
        parent_index = Heap_parent_index(index);
        parent = heap[parent_index];

        // swap
        if (parent < element) {
            heap[parent_index] = element;
            heap[index] = parent;

            // recurse
            Heap_bubble(heap, parent_index);
        }
    }
}
