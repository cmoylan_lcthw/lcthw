#include <lcthw/list.h>
#include <lcthw/dbg.h>


List *List_create()
{
    return calloc(1, sizeof(List));
}


void List_destroy(List *list)
{
    LIST_FOREACH(list, first, next, cur) {
        if (cur->prev) {
            free(cur->prev);
        }
    }

    free(list->last);
    free(list);
}


void List_clear(List *list)
{
    LIST_FOREACH(list, first, next, cur) {
        free(cur->value);
    }
}


void List_clear_destroy(List *list)
{
    LIST_FOREACH(list, first, next, cur) {
        free(cur->value);

        if (cur->prev) {
            free(cur->prev);
        }
    }

    free(list->last);
    free(list);
}


void List_push(List *list, void *value)
{
    ListNode *node = calloc(1, sizeof(ListNode));
    check_mem(node);

    node->value = value;

    // if the list is empty?
    if (list->last == NULL) {
        list->first = node;
        list->last = node;
    } else {
        list->last->next = node;
        node->prev = list->last;
        list->last = node;
    }

    list->count++;

error:
    return;
}


void *List_pop(List *list)
{
    ListNode *node = list->last;
    return node != NULL ? List_remove(list, node) : NULL;
}


/**
 * List_unshift.
 *
 * Add a value to the front of the list
 *
 * \param list the list to operate on
 * \param value the value to add
 */
void List_unshift(List *list, void *value)
{
    ListNode *node = calloc(1, sizeof(ListNode));
    check_mem(node);

    node->value = value;

    if (list->first == NULL) {
        list->first = node;
        list->last = node;
    } else {
        node->next = list->first;
        list->first->prev = node;
        list->first = node;
    }

    list->count = list->count + 1;

error:
    return;
}


void *List_shift(List *list)
{
    ListNode *node = list->first;
    return node != NULL ? List_remove(list, node) : NULL;
}


void *List_remove(List *list, ListNode *node)
{
    void *result = NULL;

    check(list->first && list->last, "List is empty.");
    check(node, "node can't be NULL");

    // list length 1?
    if (node == list->first && node == list->last) {
        list->first = NULL;
        list->last = NULL;
    }
    // first list item?
    else if (node == list->first) {
        list->first = node->next;
        check(list->first != NULL, "Invalid list, somehow got a first that is NULL.");
        list->first->prev = NULL;
    }
    // last list item
    else if (node == list->last) {
        list->last = node->prev;
        check(list->last != NULL, "Invalid list, somehow got a next that is NULL.");
        list->last->next = NULL;
    }
    // everything else
    else {
        ListNode *after = node->next;
        ListNode *before = node->prev;
        after->prev = before;
        before->next = after;
    }

    list->count = list->count - 1;
    result = node->value;
    free(node);

    return result;
error:
    return result;
}


void *List_copy(List *list)
{
    List *new_list = List_create();

    LIST_FOREACH(list, first, next, cur) {
        List_push(new_list, cur->value);
    }

    return new_list;
}


void List_join(List *first, List *second)
{
    // Update the actual nodes
    first->last->next = second->first;
    second->first->prev = first->last;

    // Update the general list properties
    first->last = second->last;
    first->count += List_count(second);
}


void *List_split(List *list, void *value)
{
    int i = 0;
    int list_count = List_count(list);
    List *chunk = List_create();

    LIST_FOREACH(list, first, next, cur) {
        //printf("-- inside split, looking at %s\n", cur->value);
        if (cur->value == value) {
            //printf("  ---splitting\n");
            // Separate the lists
            // cur->prev is the last item of the first list
            cur->prev->next = NULL;
            list->last = cur->prev;

            chunk->first = cur;
            chunk->last = list->last;

            cur->prev = NULL;

            // Update the counts
            list->count = i;
            chunk->count = list_count - i;

            // Stop looping
            break;
        }
        // Keep track of how many nodes have been traversed
        i++;
    }

    return chunk;
}


void List_print(List *list)
{
    LIST_FOREACH(list, first, next, cur) {
        printf("[%s]-", cur->value);
    }
    printf("\n");
}


void List_swap(List *list, ListNode *first, ListNode *second)
{
    // TODO: write a test for this
    ListNode *a, *b, *c, *d;

    // Use letters to keep track of the various ListNodes that will need to be
    // changed
    a = first->prev;
    b = first;
    c = second;
    d = second->next;

    first->prev = c;
    first->next = d;
    second->prev = a;
    second->next = b;
    first = c;
    second = b;

    if (a) {
        a->next = c;
    }
    if (d) {
        d->prev = b;
    }

    // update List attributes
    if (List_first(list) == b) {
        list->first = c;
    }
    if (List_last(list) == c) {
        list->last = b;
    }
}


ListNode *List_value(List *list, int index)
{
    int i = 0;
    ListNode *node = NULL;

    check(List_count(list) > index, "List is not long enough!");

    LIST_FOREACH(list, first, next, cur) {
        if (i == index) {
            node = cur;
        }
        i++;
    }

    return node;

error:
    return node;
}
