#ifndef heap_h
#define heap_h

#include <lcthw/darray.h>

#define Heap_parent_index(I) (I / 2)
#define Heap_left_index(I) (2 * I)
#define Heap_right_index(I) ((2 * I) + 1)


DArray *Heap_create();

int Heap_parent(int heap[], int index);

int Heap_left(int heap[], int index);

int Heap_right(int heap[], int index);

void Heap_heapify(int heap[], int index);

void Heap_add(int heap[], int element);

// NOTE: may not be necessary if i figure out heapify
void Heap_bubble(int heap[], int index);

static inline void Heap_print(int heap[])
{
    int i;

    printf("heap is:\n");

    for(i = 0; i < sizeof(heap); i++) {
        printf("[%d]-> %d\n", i, heap[i]);
    }
}

#endif
