#include <stdlib.h>
#include <lcthw/dbg.h>
#include <lcthw/list_algos.h>

//int List_bubble_sort(List *list, List_compare comparator)
//{
//    int i,j;
//    int count = List_count(list);
//    ListNode *cur, *next;
//
//    if (list->first && list->first->next) {
//        cur = list->first;
//        next = list->first->next;
//    } else {
//        // zero or one member list, already sorted
//        return 0;
//    }
//
//    for(i = 0; i < count; i++) {
//        cur = list->first;
//        next = list->first->next;
//
//        debug("\n\n--- bubblesort new run ---\n");
//
//        // add 1 to i because outer loop starts at 0
//        for(j= 0; j < (count - (i + 1)); j++) {
//            debug("comparing: %s and %s\n", cur->value, next->value);
//
//            if (comparator(cur->value, next->value) > 0) {
//                debug("swapping because %s > %s\n", cur->value, next->value);
//
//                // swap
//                List_swap(list, cur, next);
//            }
//
//            // update pointers
//            // If we swap or not, we need to advance the pointers
//            cur = next;
//            if (cur->next) {
//                next = cur->next;
//            }
//        }
//
//        // update pointers
//        cur = next;
//        if (cur->next) {
//            next = cur->next;
//        }
//    }
//
//    return 0;
//
// nothing uses this, but it could be used...
//error:
//    return 1;
//}
#ifdef __APPLE__
void ListNode_swap(ListNode *a, ListNode *b)
#else
inline void ListNode_swap(ListNode *a, ListNode *b)
#endif
{
      void *temp = a->value;
          a->value = b->value;
              b->value = temp;
}

int List_bubble_sort(List *list, List_compare cmp)
{
    int sorted = 1;

    if(List_count(list) <= 1) {
      return 0;  // already sorted
    }

    do {
        sorted = 1;
        LIST_FOREACH(list, first, next, cur) {
            if(cur->next) {
                if(cmp(cur->value, cur->next->value) > 0) {
                    ListNode_swap(cur, cur->next);
                    sorted = 0;
                }
            }
        }
    } while(!sorted);

    return 0;
}


#ifdef __APPLE__
List *List_merge(List *left, List *right, List_compare cmp)
#else
inline List *List_merge(List *left, List *right, List_compare cmp)
#endif
{
    List *result = List_create();
    void *val = NULL;

    while(List_count(left) > 0 || List_count(right) > 0) {
        if(List_count(left) > 0 && List_count(right) > 0) {
            if(cmp(List_first(left), List_first(right)) <= 0) {
                val = List_shift(left);
            } else {
                val = List_shift(right);
            }

            List_push(result, val);
        } else if(List_count(left) > 0) {
            val = List_shift(left);
            List_push(result, val);
        } else if(List_count(right) > 0) {
            val = List_shift(right);
            List_push(result, val);
        }
    }

    return result;
}

List *List_merge_sort(List *list, List_compare cmp)
{
    if(List_count(list) <= 1) {
      return list;
    }

    List *left = List_create();
    List *right = List_create();
    int middle = List_count(list) / 2;

    LIST_FOREACH(list, first, next, cur) {
        if(middle > 0) {
            List_push(left, cur->value);
        } else {
            List_push(right, cur->value);
        }

        middle--;
    }

    List *sort_left = List_merge_sort(left, cmp);
    List *sort_right = List_merge_sort(right, cmp);

    if(sort_left != left) List_destroy(left);
    if(sort_right != right) List_destroy(right);

    return List_merge(sort_left, sort_right, cmp);
}


//List *List_merge(List *left, List *right, List_compare comparator)
//{
//    List *result = List_create();
//    void *val = NULL;
//
//    while(List_count(left) > 0 || List_count(right) > 0) {
//        if ((List_count(left) > 0) && (List_count(right) > 0)) {
//            if (comparator(List_first(left), List_first(right)) <= 0) {
//                val = List_shift(left);
//            }
//            else {
//                val = List_shift(left);
//            }
//
//            List_push(result, val);
//        }
//        else if (List_count(left) > 0) {
//            val =  List_shift(left);
//            List_push(result, val);
//        }
//        else if (List_count(right) > 0) {
//            val =  List_shift(right);
//            List_push(result, val);
//        }
//    }
//
//    return result;
//}
//
//
//List *List_merge_sort(List *list, List_compare comparator)
//{
//    if (List_count(list) <= 1) {
//        return list;
//    }
//    
//    List *left = List_create();
//    List *right = List_create();
//    int middle = List_count(list) / 2;
//
//    LIST_FOREACH(list, first, next, cur) {
//        if (middle > 0) {
//            List_push(left, cur->value);
//        } else {
//            List_push(right, cur->value);
//        }
//
//        middle--;
//    }
//
//    List *sort_left = List_merge_sort(left, comparator);
//    List *sort_right = List_merge_sort(right, comparator);
//    printf("---- \n");
//    List_print(sort_left);
//    List_print(sort_right);
//
//    if ( sort_left != left) { List_destroy(left); }
//    if ( sort_right != right) { List_destroy(right); }
//
//    return List_merge(sort_left, sort_right, comparator);
//}
