#include <lcthw/darray_algos.h>
#include <stdlib.h>


#ifndef _quicksort_INITIAL_MAX
#define _quicksort_INITIAL_MAX 5
#endif


int DArray_qsort(DArray *array, DArray_compare cmp)
{
    int size = DArray_count(array);

    DArray *less;
    DArray *greater;

    // Already sorted
    if (size <= 1) {
        debug("array length is 1");
        return 0;
    }

    // Needs to be sorted
    else {
        debug("array length is > 1");

        // select and remove a pivot element
        // TODO: this needs to not use pop:
        //       void *pivot = DArray_remove(array, size / 2);
        void *pivot = DArray_pop(array);
        debug("pivot is %s", (char *)pivot);

        // create empty lists less and greater
        less = DArray_create(sizeof(void *), size);
        greater = DArray_create(sizeof(void *), size);

        void *elm;

        for (; elm != NULL; elm = DArray_pop(array)) {
            check(elm != NULL, "elm is NULL");

            debug("elm and pivot: %s, %s", (char *)elm, (char *)pivot);

            // Elm is less than pivot
            if (cmp(&pivot, &elm) > 0) {
                debug("pushing elm onto less");
                DArray_push(less, elm);
            }
            // Elm is greater than pivot
            else {
                debug("pushing elm onto greater");
                DArray_push(greater, elm);
            }
        }

        check(DArray_count(array) == 0, "array is not empty");

        int rc;
        rc = DArray_qsort(less, cmp);
        check(rc == 0, "failed to sort less");
        rc = DArray_qsort(greater, cmp);
        check(rc == 0, "failed to sort greater");

        // return concatenate(quicksort(less), list(pivot), quicksort(greater)) // two recursive calls
        for (; elm != NULL; elm = DArray_first(less)) {
            DArray_push(array, elm);
        }

        DArray_push(array, pivot);

        for (; elm != NULL; elm = DArray_first(greater)) {
            DArray_push(array, elm);
        }


        DArray_destroy(less);
        DArray_destroy(greater);

        return 0;
    }


error:
    if (array) DArray_destroy(array);
    if (less) DArray_destroy(less);
    if (greater) DArray_destroy(greater);

    return -1;
}


int DArray_slow_qsort(DArray *array, DArray_compare cmp)
{
  debug( ">>> DArray_quicksort\n");
  check(array != NULL, "array is NULL\n");
  debug( "array: %p\n", array);

  // declare here for error handling
  DArray *less;
  DArray *more;

  if (DArray_count(array) <= 1) {
    // Already sorted
    debug( "array less/equal 1\n");
    return 0;

  } else {
    // recursive-case: array is longer than 1
    debug( "array more than 1\n");

    // pick a pivot
    void *pivot = DArray_pop(array);
    check(pivot != NULL, "pivot is NULL");
    debug("length of array: %d", DArray_count(array));

    // make a list of larger and smaller elements
    less = DArray_create(sizeof(void *), _quicksort_INITIAL_MAX);
    more = DArray_create(sizeof(void *), _quicksort_INITIAL_MAX);
    debug("create less and more");
    void *cur;
    int i;

    for (cur = DArray_pop(array); cur != NULL; cur = DArray_pop(array)) {

      check(cur != NULL, "cur is NULL");
      debug("length of array: %d; i: %d", DArray_count(array), i);

      debug("comparing cur to pivot");
      debug("cur: %s", (char *)cur);
      debug("pivot: %s", (char *)pivot);

      debug("address of comparator: %p", cmp);
      debug("cmp(&pivot, &cur): %d", cmp(&pivot, &cur));

      if (cmp(&pivot, &cur) > 0) {
        debug("cur is less than the pivot");
        DArray_push(less, cur);
      } else {
        debug("cur is more than the pivot");
        DArray_push(more, cur);
      }
    }

    check(DArray_count(array) == 0, "array not empty");

    // get lengths before
    int len_less_before = DArray_count(less);
    int len_more_before = DArray_count(more);

    // sort the sublists
    int rc1 = DArray_slow_qsort(less, cmp);
    int rc2 = DArray_slow_qsort(more, cmp);
    check(rc1 == 0, "failed to sort less sublist");
    check(rc2 == 0, "failed to sort more sublist");

    // check lengths
    check(len_less_before == DArray_count(less), "length of less changes");
    debug("old len(more): %d; new len(more): %d", len_more_before, DArray_count(more));
    check(len_more_before == DArray_count(more), "length of more changes");

    // at this point less and more are already sorted

    // write all elements back into the old array
    for( i = 0, cur = DArray_first(less);
      i < DArray_count(less);
      i++, cur = DArray_get(less, i)) {

      debug("pushing from less, length of less: %d; i: %d", DArray_count(less), i);
      DArray_push(array, cur);
    }

    DArray_push(array, pivot);
    debug("pushing from pivot");

    for(i = 0, cur = DArray_first(more);
      i < DArray_count(more);
      i++, cur = DArray_get(more, i)) {

      debug("pushing from more");
      DArray_push(array, cur);
    }

    // check size
    int s_array = DArray_count(array);
    int s_less = DArray_count(less);
    int s_more = DArray_count(more);
    check(s_array = (s_less + s_more + 1), "number of elements changes");

    // destroy the temporary arrays
    DArray_destroy(less);
    DArray_destroy(more);

    // give feedback
    #ifndef NDEBUG
    int k;
    for(k=0; k<DArray_count(array); k++) {
      debug("element %d: %s", k, DArray_get(array, k));
    }
    #endif

    return 0;
  }

error:
    // free all allocated memory
    if(array) DArray_destroy(array);
    if(less) DArray_destroy(less);
    if(more) DArray_destroy(more);

    return 1;
}


int DArray_heapsort(DArray *array, DArray_compare cmp)
{
    //return heapsort(array->contents, DArray_count(array), sizeof(void *), cmp);

    //function heapSort(a, count) is
    //     input:  an unordered array a of length count
    //
    //     (first place a in max-heap order)
    //     heapify(a, count)
    DArray_heapify(array, DArray_count(array));
    //
    //     end := count-1 //in languages with zero-based arrays the children are 2*i+1 and 2*i+2
    //     while end > 0 do
    //         (swap the root(maximum value) of the heap with the last element of the heap)
    //         swap(a[end], a[0])
    //         (decrease the size of the heap by one so that the previous max value will
    //         stay in its proper placement)
    //         end := end - 1
    //         (put the heap back in max-heap order)
    //         siftDown(a, 0, end

    return 0;

error:
    return -1;
}


int DArray_heapify(DArray *array, int count)
{
//function heapify(a, count) is
//     (start is assigned the index in a of the last parent node)
//     start := (count - 2 ) / 2
    int start = (count - 2) / 2;
//
//     while start ≥ 0 do
//         (sift down the node at index start to the proper place such that all nodes below
//          the start index are in heap order)
//         siftDown(a, start, count-1)
//         start := start - 1
//     (after sifting down the root all nodes/elements are in heap order)
    while (start >= 0) {
        DArray_sift_down(array, start, (count - 1));
        start -= 1;
    }
    return 0;
}


int DArray_sift_down(DArray *array, int start, int end)
{
//function siftDown(a, start, end) is
//     input:  end represents the limit of how far down the heap
//                   to sift.
//     root := start
    void *swap, *child;
    void *root = start;

////     while root * 2 + 1 ≤ end do          (While the root has at least one child)
//    while ((root * 2 + 1) <= end) {
////         child := root * 2 + 1        (root*2 + 1 points to the left child)
//        child = root * 2 + 1;
////         swap := root        (keeps track of child to swap with)
//        swap = root;
////         (check if root is smaller than left child)
////         if a[swap] < a[child]
//        //if ()
////             swap := child
////         (check if right child exists, and if it's bigger than what we're currently swapping with)
////         if child+1 ≤ end and a[swap] < a[child+1]
////             swap := child + 1
////         (check if we need to swap at all)
////         if swap != root
////             swap(a[root], a[swap])
////             root := swap          (repeat to continue sifting down the child now)
////         else
////             return
//    }
    return 0;
}


int DArray_mergesort(DArray *array, DArray_compare cmp)
{
    //return mergesort(array->contents, DArray_count(array), sizeof(void *), cmp);
    return 0;

}

