#include "minunit.h"
#include <lcthw/heap.h>


// NOTE: first heap element is just a placeholder so that the actual heap
//       is 1-indexed.
static int heap[] = {0, 20, 14, 17, 8, 6, 9, 4, 1};

//char *test_create()
//{
//    array = DArray_create(sizeof(int), 100);
//    mu_assert(array != NULL, "DArray_create failed.");
//    mu_assert(array->contents != NULL, "contents are wrong in darray");
//    mu_assert(array->end == 0, "end isn't at the right spot");
//    mu_assert(array->element_size == sizeof(int), "element size is wrong");
//    mu_assert(array->max == 100, "wrong max length on initial size");
//
//    return NULL;
//}

char *test_parent()
{
    int parent;

    // heap[3] -> 8
    parent = Heap_parent(heap, 3);
    mu_assert(parent == 14, "wrong parent element");

    // heap[7] -> 1
    parent = Heap_parent(heap, 7);
    mu_assert(parent == 8, "wrong parent element");

    // heap[6] -> 4
    parent = Heap_parent(heap, 6);
    mu_assert(parent == 17, "wrong parent element");

    // heap[5] -> 9
    parent = Heap_parent(heap, 5);
    mu_assert(parent == 17, "wrong parent element");

    return NULL;
}

char *test_left()
{
    int left;

    // heap[1] -> 14
    left = Heap_left(heap, 1);
    mu_assert(left == 8, "wrong left element for heap[1]");

    // heap[3] -> 8
    left = Heap_left(heap, 3);
    mu_assert(left == 1, "wrong left element for heap[3]");

    // heap[2] -> 17
    left = Heap_left(heap, 2);
    mu_assert(left == 9, "wrong left element for heap[2]");

    return NULL;
}

char *test_right()
{
    int right;

    // heap[1] -> 14
    right = Heap_right(heap, 1);
    mu_assert(right == 6, "wrong right element for heap[1]");

    // heap[2] -> 17
    right = Heap_right(heap, 2);
    mu_assert(right == 4, "wrong right element for heap[2]");

    return NULL;
}

char *test_heapify()
{
    // NOTE: this test is correct, the implementation is not
    int heap[] = {0, 14, 20, 17};
    Heap_heapify(heap, 0);

    mu_assert(heap[0] == 20, "wrong element at heap[0]");
    mu_assert(heap[1] == 14, "wrong element at heap[1]");

    return NULL;
}

char *test_bubble()
{
    int heap[] = {0, 10, 9, 11};
    Heap_bubble(heap, 3);

    mu_assert(heap[1] == 11, "last element did not bubble");

    return NULL;
}

char *test_add()
{
    int heap[] = {0, 10, 9, 8};
    int element = 15;
    Heap_print(heap);
    Heap_add(heap, element);
    Heap_print(heap);
    // 0, 15, 10, 9, 8

    mu_assert(sizeof(heap) == 5, "heap size did not increase");
    mu_assert(heap[1] == 15, "heap not heapified");

    return NULL;
}

//char *test_expand_contract()
//{
//    int old_max = array->max;
//    DArray_expand(array);
//    mu_assert((unsigned int)array->max == old_max + array->expand_rate, "Wrong size after expand");
//
//    DArray_contract(array);
//    mu_assert((unsigned int)array->max == array->expand_rate + 1, "Should stay at the expand_rate at least");
//
//    DArray_contract(array);
//    mu_assert((unsigned int)array->max == array->expand_rate + 1, "Should stay at the expand_rate at least");
//
//    return NULL;
//}
//
//char *test_push_pop()
//{
//    int i = 0;
//    for(i = 0; i < 1000; i++) {
//        //int *val = DArray_new(array);
//        int *val = i * 333;
//        DArray_push(array, val);
//    }
//
//    mu_assert(array->max == 1201, "Wrong max size");
//
//    for(i = 999; i >= 0; i--) {
//        int *val = DArray_pop(array);
//        printf("iteration %d\n", i);
//        mu_assert(val != NULL, "Shouldn't get a NULL");
//        mu_assert(*val == i * 333, "Wrong value");
//        DArray_free(val);
//    }
//
//    return NULL;
//}

char *all_tests()
{
    mu_suite_start();

    mu_run_test(test_parent);
    mu_run_test(test_left);
    mu_run_test(test_right);
    //mu_run_test(test_heapify);
    mu_run_test(test_bubble);
    mu_run_test(test_add);

    //mu_run_test(test_expand_contract);
    ////mu_run_test(test_push_pop);
    //mu_run_test(test_destroy);

    return NULL;
}

RUN_TESTS(all_tests);
